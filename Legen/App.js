import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import firebase from 'firebase';



import Login from './screen/login'
import HomeScreen from './screen/home'

import Member1Screen from './screen/member1';
import Member2Screen from './screen/member2';
import Member3Screen from './screen/member3';
import Doc2 from './screen/doc2';
import Doc3 from './screen/doc3';

const AppNavigator = createStackNavigator(
  {
    Login : Login,
    Home: HomeScreen,
    Member1: Member1Screen,
    Member2: Member2Screen,
    Member3: Member3Screen,
    Doc2 : Doc2,
    Doc3 : Doc3
  },
  {
    initialRouteName: "Home"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    
    return (
      
      <AppContainer />
    )

  }
}


