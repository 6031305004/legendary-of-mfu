import * as firebase from 'firebase'
var firebaseConfig = {
    apiKey: "AIzaSyB9I98pH8PTqTpuSW1QyVS6ODAF4iajSOc",
    authDomain: "projectdb-database.firebaseapp.com",
    databaseURL: "https://projectdb-database.firebaseio.com",
    projectId: "projectdb-database",
    storageBucket: "projectdb-database.appspot.com",
    messagingSenderId: "1027103006921",
    appId: "1:1027103006921:web:96f720e22600ca8a"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  export const database = firebase.database();
  