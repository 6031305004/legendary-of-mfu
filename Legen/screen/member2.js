import React from 'react';
import { Button } from 'react-native';
import { ScrollView, Image, StyleSheet, Text, View , TextInput} from 'react-native';
import HeaderBar from '../components/header';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Card from '../components/card'
import Card2 from '../components/card2';

export default class Member2Screen extends React.Component {
  render() {
    let pic1 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/56847988_2353292248055697_9008872397905854464_o.jpg?_nc_cat=102&_nc_ht=scontent.fbkk10-1.fna&oh=8fc00318da008034dd413719e0db6868&oe=5D2E3913' }
    let pic2 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/43666911_1111008295724263_630723253156970496_n.jpg?_nc_cat=105&_nc_ht=scontent.fbkk10-1.fna&oh=d038427e1edde5c999662983852e3833&oe=5D3285A3' }
    let pic3 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/11230591_949200078477441_2726203477443511519_n.jpg?_nc_cat=101&_nc_ht=scontent.fbkk10-1.fna&oh=f09bab0e7a0e699c3f1fe25a846dcf6b&oe=5D769C73' }  
    return (
      <ScrollView>
          <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Doc2')}>
          <Card2 img={pic1} title='Doctor1' />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Doc3')}>
          <Card2 img={pic2} title='Doctor2' />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Member3')}>
          <Card2 img={pic3} title='Doctor3' />
        </TouchableOpacity>

      </ScrollView>
    );
  }
}


