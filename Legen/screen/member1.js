import React from 'react';
import { Button } from 'react-native';
import { ScrollView, Image, StyleSheet, Text, View , TextInput} from 'react-native';
import HeaderBar from '../components/header';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Card from '../components/card'

export default class Member1Screen extends React.Component {
  render() {

    let pic = { uri: 'https://imageog.flaticon.com/icons/png/512/25/25694.png?size=1200x630f&pad=10,10,10,10&ext=png&bg=FFFFFFFF' }
    let pic2 = { uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAe1BMVEUAAAD////p6elubm43NzcWFhaGhoaZmZne3t7a2trJycn8/Pzk5OT29vbs7Ozx8fHExMSsrKwiIiJLS0t3d3eysrJ+fn64uLhTU1OhoaGTk5OmpqbOzs45OTlFRUW+vr4tLS1bW1sdHR1jY2MPDw9XV1d8fHwvLy8/Pz+OtLSVAAAFNElEQVR4nO3a7XaiOhQGYIOKaAApfoCIA6217f1f4dkBj5CQBNHF1DXrfX5NMQQ2CclOmMkEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfk++2v32LYyMMee3b2FkfzHCj8vXIYmiKDl8pZ89ZedpXXaXnU9PXpYx/8ka7vP5lYesERZnS+Fp0SobLBc/yu9lnufvtqtlnhf9/29Rg84z0dzMizyp/3VYV3fLneXSu979cmM4a+80Za+nFTO5iMvYeqY/W9jSKbenwgyejK02D+seMhVBrZPtbF4f3ix8cY1Md87JEz+t3FNdNn4/VgEv5FJHW9/bcMaaLkJtGK00ngvtau4w0RneRIspvTL1GWfH7il70WRH+TW9LEUFc+lYRE/BdNWAMbf5c9T3sI6QbmY97f540LXigg5GH52yU+qtjtwtqanf9Ff15V9GHUurCBN6/t17Jhm14p/uIW3fjakZw7h9pAyllmrQA83bf48dYe4qF2xJ1J+mFKD2rknOmCcdSKW37Ya6hi916LEjdALzaxBT35spfy9MhT+XarekETP4oxZzGQ/kGXTsCEnnLm7oebcHm8Lc3OREA4g8wYjmUvr/mRpWmSnHj9CS99Kw3mrglArH5sJiElSm6c4jma27L+f4EWpHmSunPfFGTDd9yKWV9vGUB+hrBtjRI4xsBVatPjzjjNueRjVXKrVRStEee3PdJDl6hHtbAUpNbjOl2/M0CGeB0o1F+nKrgTILv5vSjx6hJX0U81/z2uStezWg926rHKIBlV1TUJGsqjn6ZPwIA+s6yW062XzNeNlT31YzblEWtK4695nSPd2CZewI7Tmh20yAP52RUlOfOusLRX3iT2DIcV4mwmn/ayhexLA7GFEqUFQvhH4kHjvC7jNva0VIr+Sht0Kvte5rUOsdtMNo5WUipGE1K2d2Iv9Ou7WcqhWtKY6XiXBnWosrdIPJWSywTenQvxEhV9dWLS8TIU3Xx+kdym4tf7gI3XSll4nwYBjs77gKpdvH3DgUv0yErnFXos9SDKNx+PKzxbt5a8muqDvohnN9DvwyEX4/uCdGWVudjlPKoJtKXidCsdbTzOZ9ts0mQsZ4oMnzXyfCnWGL2OrCW8uNRN2EqgQsHFzt3QZF+G4e8I3ErkUr19Ombj4LyqH13m1QhGILwvq5ZTLf7ZTU1RNJdyN2NANy1FftM4ZFmPWtn47qhLCiM6T150azhHp8or3DsAjFTtOXpfAPV9bTFHH4LZcRA6rSYhfrHuWTBkZIt8ct24mesumz100PVCNXvqmuGTd9yHvawAjFYGge2QslL7sw3tm1mVRDcigPqG+P5hJ3GBqhSMB8QysmSkYgtsC1S+ZcfZ3LwPw15FmDI/yk8TTUjXxlruzgf/jyMNpCGWoiHRBftKybmo8bHOFkLj6G7r475ajBcmmHZmUeeE9crZU6OHtTN/2mxs/sAwyPsF4JB7t2O24WjvguLJWidyssTbWKJb+89VpQK4ZZKyk8ufQoHdtXkvs8EuHkXH3ID1cHd7v9cg+RI9a3eaqeZxsfuz9nohLuJdl+u90vCi8Q+x5Z3/956UdrU3vW62pHi2mu7FxEF6UA625+S6iJuZyEl29crtPPuinsAzap+eNhdd00LXXHYzdyrnfiJdvOo96kPfVONpu0s1CZ7pbXOterwwPLmBHEsf1b1CPmcfx83wQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgH/ef3xHNmXk3+T3AAAAAElFTkSuQmCC' }


    return (
      <ScrollView>
        <Image
          style={{width: 400, height: 200}}
          source={{uri: 'https://cdn.lynda.com/course/373523/373523-636190611757485494-16x9.jpg'}}
        />
        <Text></Text>
        <Text></Text>
        <Text> Name : Guntapong Surname: Meepunya</Text>
        <Text> Address : 413/21 ต.ท่าสุด อ.เมืองเชียงราย จ.เชียงราย 57100</Text>
        <Text></Text>
        
        <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Home')}>
             <Card img={pic} title='Back to Home ' />
             </TouchableOpacity>

             <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Member2')}>
             <Card img={pic2} title='Next' />
             </TouchableOpacity>
        
      </ScrollView>
    );
  }
}

