import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View, FlatList } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Card3 from '../components/card3';
import Card2 from '../components/card2';
import login from './login';
import {database} from './firebase'

export default class HomeScreen extends React.Component {
  state = {
    item: [],
  }
  componentDidMount(){
    var userRef = database.ref('/users/')
    userRef.on('value', (snapshot) => {
      let data = snapshot.val()
     let item = Object.values(data)
      this.setState({data:item})
    })
  }

  render() {
    let pic1 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/56847988_2353292248055697_9008872397905854464_o.jpg?_nc_cat=102&_nc_ht=scontent.fbkk10-1.fna&oh=8fc00318da008034dd413719e0db6868&oe=5D2E3913' }
    let pic2 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/43666911_1111008295724263_630723253156970496_n.jpg?_nc_cat=105&_nc_ht=scontent.fbkk10-1.fna&oh=d038427e1edde5c999662983852e3833&oe=5D3285A3' }
let pic3 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/11230591_949200078477441_2726203477443511519_n.jpg?_nc_cat=101&_nc_ht=scontent.fbkk10-1.fna&oh=f09bab0e7a0e699c3f1fe25a846dcf6b&oe=5D769C73' }    
let pic4 = { uri: 'http://vichakarndeedee.com/images/button_register_large.png' }
    return (
      <ScrollView>
        <HeaderBar headtitle='Patient Appointment' />
        

        <FlatList data={this.state.data} renderItem={({item}) => 
         <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
         <Text style={{fontSize:20}}>Email: {item.email}</Text>
         <Text style={{fontSize:20}}>Password: {item.password}</Text>
       </View>    
      }/>
         
          
        
        { <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Login')}>
          <Card3 img={pic4} title='Register' />
        </TouchableOpacity> }




      </ScrollView>
    );
  }
}


