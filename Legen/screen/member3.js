import React from 'react';
import { ScrollView,Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card3 from '../components/card3';

export default class Member3Screen extends React.Component {
  render() {
    let pic = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/11230591_949200078477441_2726203477443511519_n.jpg?_nc_cat=101&_nc_ht=scontent.fbkk10-1.fna&oh=f09bab0e7a0e699c3f1fe25a846dcf6b&oe=5D769C73' }

    return (

      <ScrollView>
        <Card3 img = {pic} title='ดร. Doctor 3'/>
      </ScrollView>
      
    );
}
}
