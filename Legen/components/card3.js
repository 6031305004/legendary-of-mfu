import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
// import HeaderBar from './components/header';


export default class Card3 extends React.Component {
  render() {
    let pic3 = { uri: 'https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/57541827_2139645416127349_9027517134405632000_n.jpg?_nc_cat=108&_nc_ht=scontent.fbkk10-1.fna&oh=b9d335c7b8a7479db8d32fe8521ddc08&oe=5D2BD782' }

    return (
    //   <View>
    //     <HeaderBar />
        <View style={{ borderColor: 'gray', borderWidth: 0.5 }}>
          <Image style={{ height: 200 }}
            source={this.props.img} />
          <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 10 }}>
            {this.props.title}
          </Text>
        </View>
    //   </View>
    );
  }
}


